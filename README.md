## DAN-IT-Digital Design

Check out my website  [here](https://dan-it-digital-design-ya-fluffy-dfb1ea3498b3a83f2229cb518e38b10.gitlab.io/).

+ Styling for the project is written using the SASS/SCSS preprocessor.
+ Element classes are written according to the BEM methodology.
+ The task manager gulp was used to assemble the project.

